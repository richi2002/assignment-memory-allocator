#include "all_tests.h"
#include <stdbool.h>
#include <stdio.h>


#define HEAP_INIT 10000

int main() {
    printf("Initialize Heap:\n");

    void * heap = heap_init(HEAP_INIT);

    if (heap == NULL) {
        err("There is no heap\n");
    }

    debug_heap(stdout, heap);

    int passed_tests = 0;

    if (test1(heap)) passed_tests++;
    if (test2(heap)) passed_tests++;
    if (test3(heap)) passed_tests++;
    if (test4(heap)) passed_tests++;
    if (test5(heap)) passed_tests++;

    if (passed_tests == 5) {
        printf("ALL 5 TESTS PASSED");
        return 0;
    }

    return 1;
}

