#define _DEFAULT_SOURCE
#include "mem.h"
#include <unistd.h>
#include <stdbool.h>



bool test1(void* heap) {

  int* p = malloc(sizeof(int));
  if (p == NULL) {
    return false;
  }
  *p = 10;
  free(p);

  if (test2(NULL)) {
    printf("Test 2 passed\n");
  } else {
    printf("Test 2 failed\n");
  }


  return true;
}


// Declare a function prototype for the mapper function
void* mapper(void* heap, size_t size);

bool test2(void* heap) {
    // Allocate a block of memory to use as the heap
  void* heap = malloc(1000);
  // Allocate three blocks of memory from the heap
  void* block1 = mapper(heap, 100);
  void* block2 = mapper(heap, 200);
  void* block3 = mapper(heap, 300);

  // Release the second and third blocks of memory
  mapper(block2, 0);
  mapper(block3, 0);

  // Try to allocate a block of memory that is the combined size of the second and third blocks
  void* combined_block = mapper(heap, 500);

    if (test2(heap)) {
    printf("Test passed!\n");
  } else {
    printf("Test failed.\n");
  }

  // Free the heap memory
  free(heap);
  // Check if the combined block was successfully allocated
  return combined_block != NULL;

  
}
  

bool test3(void* heap) {
    void* heap = malloc(1024);
    bool result = test3(heap);

  // Allocate two blocks of memory on the heap
  void* block1 = malloc(64);
  void* block2 = malloc(128);

  // Release both blocks of memory
  free(block1);
  free(block2);

  // Check that the blocks are available for reuse
  void* block3 = malloc(64);
  if (block3 != block1) {
    return false;
  }
  void* block4 = malloc(128);
  if (block4 != block2) {
    return false;
  }

  // Clean up and return success
  free(block3);
  free(block4);

  if (result) {
    printf("Test 3 passed\n");
  } else {
    printf("Test 3 failed\n");
  }

  return true;
}



bool test4(void* heap) {
    #define HEAP_SIZE4 1048576 // 1MB
    void* heap = NULL;
    // Allocate a large block of memory
    char* p = (char*) malloc(HEAP_SIZE4);
    if (p == NULL) {
        return false;
    }

    for (int i = 0; i < HEAP_SIZE4; i++) {
        p[i] = (char) i;
    }

    for (int i = 0; i < HEAP_SIZE4; i++) {
        if (p[i] != (char) i) {
            return false;
        }
    }

    char* q = (char*) malloc(HEAP_SIZE4);
    if (q == NULL) {
        return false;
    }

    for (int i = 0; i < HEAP_SIZE4; i++) {
        if (q[i] != 0) {
            return false;
        }
    }

    free(p);
    free(q);

    if (test4(heap)) {
        printf("Test 4 passed\n");
    } else {
        printf("Test 4 failed\n");
    }

    return true;
    
}


bool test5(void* heap) {

    #define HEAP_SIZE5 100

    void* heap = malloc(HEAP_SIZE5);
    if (heap == NULL) {
        return 1;
    }

    // Try to allocate more memory than the heap size
    void* ptr1 = malloc(HEAP_SIZE5 + 1);
    if (ptr1 == NULL) {
        return false;
    }

    // Check if the allocated memory is in a different address range than the heap
    if ((uintptr_t)ptr1 >= (uintptr_t)heap && (uintptr_t)ptr1 < (uintptr_t)heap + HEAP_SIZE5) {
        return false;
    }

    // Try to realloc the original pointer with a larger size
    void* ptr2 = realloc(ptr1, HEAP_SIZE5 + 2);
    if (ptr2 == NULL) {
        return false;
    }

    // Check if the reallocated memory is in a different address range than the heap
    if ((uintptr_t)ptr2 >= (uintptr_t)heap && (uintptr_t)ptr2 < (uintptr_t)heap + HEAP_SIZE5) {
        return false;
    }

    // Check if the reallocated memory is the same as the original pointer
    if (ptr1 != ptr2) {
        return false;
    }

    free(ptr2);

    if (test5(heap)) {
        printf("Test 5 passed\n");
    } else {
        printf("Test 5 failed\n");
    }

    free(heap);


    return true;

}

