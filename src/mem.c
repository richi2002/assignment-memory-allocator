#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
struct region alloc_region(void const *addr, size_t query) {
  struct region region;
  size_t reg_act_size = region_actual_size(query);
  void *reg_addr = map_pages(addr, query, MAP_FIXED);
  if (reg_addr == MAP_FAILED) {
    reg_addr = map_pages(addr, reg_act_size, 0);
  }
  region.address = reg_addr;
  region.size = query;
  region.is_free = false;
  lock_init(reg_addr, (block_size){reg_act_size}, NULL);
  return region;
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  if (block_splittable(block, query)) {
    char block_addr_new = (char)(block) + offsetof(struct block_header, contents) + query;
    block_size bs = {block->capacity.bytes - query};
    block_init(block_addr_new, bs, NULL);
    (*block).capacity.bytes = query;
    (*block).next = block_addr_new;
    return true;
  }
  return false;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  if (block->next && mergeable(block, ((*block).next))) {
    struct block_header* next = (*block).next;
    struct block_size next_size = size_from_capacity(next->capacity);
    (*block).next = (*next).next;
    (*block).capacity.bytes += next_size.bytes;
    return true;
  } else {return false;}
  
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {

  struct block_search_result result;

  if (!block) {
  result.type = BSR_CORRUPTED;
  return result;
  }

  struct block_header * cur_block = block;
  struct block_header * new_block = NULL;

  struct block_search_result result;
  for (cur_block = head_block; cur_block != NULL; cur_block = (*cur_block).next) {
    if (block_is_big_enough(sz, block) && ((*cur_block).is_free)) {
    result.type = BSR_FOUND_GOOD_BLOCK;
    result.block = cur_block;
    return result;
    }
  }
  new_block = cur_block;
  cur_block = (*cur_block).next;
  }

  return (struct block_search_result) {.type = BSR_REACHED_END_NOT_FOUND, .block = new_block};

}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {

  struct block_search_result block_found = find_good_or_last(block, query);
	block_found.type == BSR_FOUND_GOOD_BLOCK ? split_if_too_big(block_found.block, query) : 0;
	return block_found;

}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  const void* addr;
  const struct region region;

  addr = (void*) ((*last).capacity.bytes + ((*last).contents));
  region = alloc_region(addr, query);

  if (region_is_invalid(&region)) {
    return NULL;
  }

  (*last).next = region.addr;

  if (!try_merge_with_next(last)) {
    return region.addr;
  }

  return last;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {

  query = size_max(query, BLOCK_MIN_CAPACITY);
	struct block_search_result block_found = try_memalloc_existing(query, heap_start);

  switch (block_found.type) {
  case BSR_CORRUPTED:
    block_found.block = heap_init(query);
    split_if_too_big(block_found.block, query);
    break;
  case BSR_REACHED_END_NOT_FOUND:
    block_found.block = grow_heap(block_found.block, query);
    split_if_too_big(block_found.block, query);
    break;
  }

  (*block_found.block).is_free = false;
  return block_found.block;

}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  (*header).is_free = true;

  for (Node* curr = header; curr != NULL; curr = curr->next) {
    try_merge_with_next(curr);
}

}
